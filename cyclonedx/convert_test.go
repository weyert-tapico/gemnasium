package cyclonedx_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	cyclonedx "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestCycloneDX(t *testing.T) {
	t.Run("ToSBOMs", func(t *testing.T) {
		timeStamp := time.Now()

		scannerFiles := []scanner.File{
			{
				Path:           "Gemfile.lock",
				PackageManager: "bundler",
				RootDir:        "/qa/fixtures/multi-project/subdirs/ruby-project-1",
				PackageType:    "gem",
				Packages: []parser.Package{
					{Name: "pg", Version: "0.8.0"},
					{Name: "puma", Version: "2.16.0"},
				},
			},
			{
				Path:           "javascript-project/yarn.lock",
				PackageManager: "yarn",
				RootDir:        "/qa/fixtures/multi-project/subdirs/yarn-project",
				PackageType:    "npm",
				Packages: []parser.Package{
					{Name: "acorn", Version: "4.0.4"},
					{Name: "acorn", Version: "3.3.0"},
					{Name: "acorn", Version: "4.0.11"},
					{Name: "@angular/animations", Version: "4.4.6"},
				},
			},
		}

		toolInfo := cyclonedx.ToolInfo{Name: "Gemnasium", Vendor: "GitLab", Version: "1.2.3"}

		actual := cyclonedx.ToSBOMs(scannerFiles, &timeStamp, toolInfo)

		expected := []cyclonedx.SBOM{
			{
				BomFormat:   "CycloneDX",
				SpecVersion: "1.4",
				// force the SerialNumber to be equal, since they're a moving target
				SerialNumber: actual[0].SerialNumber,
				Version:      1,
				Metadata: cyclonedx.Metadata{
					// force the Timestamp to be equal to the actual report Timestamp, since it's a moving target
					Timestamp: actual[0].Metadata.Timestamp,
					Tools: []cyclonedx.Tool{
						{Vendor: "GitLab", Name: "Gemnasium", Version: "1.2.3"},
					},
					Authors: []cyclonedx.Author{
						{Name: "GitLab", Email: "support@gitlab.com"},
					},
					Properties: []cyclonedx.MetadataProperty{
						{Name: "gitlab:dependency_scanning:input_file", Value: "Gemfile.lock"},
						{Name: "gitlab:dependency_scanning:package_manager", Value: "bundler"},
					},
				},
				Components: []cyclonedx.Component{
					{Name: "pg", Version: "0.8.0", PURL: "pkg:gem/pg@0.8.0", ComponentType: "library", BomRef: "pkg:gem/pg@0.8.0"},
					{Name: "puma", Version: "2.16.0", PURL: "pkg:gem/puma@2.16.0", ComponentType: "library", BomRef: "pkg:gem/puma@2.16.0"},
				},
				InputFilePath:  "Gemfile.lock",
				OutputFilePath: "gl-sbom-gem-bundler.cdx.json",
				PackageManager: "bundler",
				PackageType:    "gem",
			},
			{
				BomFormat:   "CycloneDX",
				SpecVersion: "1.4",
				// force the SerialNumber to be equal, since they're a moving target
				SerialNumber: actual[1].SerialNumber,
				Version:      1,
				Metadata: cyclonedx.Metadata{
					// force the Timestamp to be equal to the actual report Timestamp, since it's a moving target
					Timestamp: actual[1].Metadata.Timestamp,
					Tools: []cyclonedx.Tool{
						{Vendor: "GitLab", Name: "Gemnasium", Version: "1.2.3"},
					},
					Authors: []cyclonedx.Author{
						{Name: "GitLab", Email: "support@gitlab.com"},
					},
					Properties: []cyclonedx.MetadataProperty{
						{Name: "gitlab:dependency_scanning:input_file", Value: "javascript-project/yarn.lock"},
						{Name: "gitlab:dependency_scanning:package_manager", Value: "yarn"},
					},
				},
				Components: []cyclonedx.Component{
					{Name: "@angular/animations", Version: "4.4.6", PURL: "pkg:npm/@angular/animations@4.4.6", ComponentType: "library", BomRef: "pkg:npm/@angular/animations@4.4.6"},
					{Name: "acorn", Version: "3.3.0", PURL: "pkg:npm/acorn@3.3.0", ComponentType: "library", BomRef: "pkg:npm/acorn@3.3.0"},
					{Name: "acorn", Version: "4.0.11", PURL: "pkg:npm/acorn@4.0.11", ComponentType: "library", BomRef: "pkg:npm/acorn@4.0.11"},
					{Name: "acorn", Version: "4.0.4", PURL: "pkg:npm/acorn@4.0.4", ComponentType: "library", BomRef: "pkg:npm/acorn@4.0.4"},
				},
				InputFilePath:  "javascript-project/yarn.lock",
				OutputFilePath: "javascript-project/gl-sbom-npm-yarn.cdx.json",
				PackageManager: "yarn",
				PackageType:    "npm",
			},
		}

		require.Equal(t, expected, actual)
	})
}
