package libfinder

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"

// Library represents a vendored library
type Library struct {
	Path        string
	PackageType parser.PackageType
	Package     parser.Package
}
