package maven

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder/exportpath"
)

const (
	flagMavenOpts = "maven-opts"

	pathGemnasiumPluginOutput = "gemnasium-maven-plugin.json"

	mavenPluginVersion = "0.4.0"
)

// Builder generates dependency lists for maven projects
type Builder struct {
	MavenOpts string
}

// Flags returns the CLI flags that configure the mvn command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagMavenOpts,
			Usage:   "Optional CLI arguments for the maven install command",
			Value:   "-DskipTests --batch-mode",
			EnvVars: []string{"MAVEN_CLI_OPTS"},
		},
	}
}

// Configure configures the mvn command
func (b *Builder) Configure(c *cli.Context) error {
	b.MavenOpts = c.String(flagMavenOpts)
	return nil
}

// Build generates graph exports for all modules of a multi-module Maven project.
// It returns the export path for the root module, and the export paths for its sub-modules, if any.
func (b Builder) Build(input string) (string, []string, error) {
	// install maven dependencies
	if err := b.installDeps(input); err != nil {
		return "", []string{}, err
	}

	// export dependency graphs
	paths, err := b.listDeps(input)
	if err != nil {
		return "", []string{}, err
	}

	return exportpath.Split(paths, filepath.Dir(input))
}

// installDeps installs the maven dependencies
func (b Builder) installDeps(input string) error {
	args := []string{"install"}
	args = append(args, strings.Fields(b.MavenOpts)...)
	cmd := exec.Command("mvn", args...)
	cmd.Dir = filepath.Dir(input)
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

// listDeps exports the dependency list to JSON using the Gemnasium Maven Plugin.
// It returns the paths to the JSON export created for the root project and its sub-projects, if any.
func (b Builder) listDeps(input string) ([]string, error) {
	// run gemnasium-maven-plugin dump-dependencies task
	task := "com.gemnasium:gemnasium-maven-plugin:" + mavenPluginVersion + ":dump-dependencies"
	args := []string{task}
	args = append(args, strings.Fields(b.MavenOpts)...)
	cmd := exec.Command("mvn", args...)
	cmd.Dir = filepath.Dir(input)
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	return exportpath.ExtractMaven(output)
}

func init() {
	builder.Register("maven", &Builder{})
}
