package builder

import (
	"errors"
	"fmt"

	"github.com/urfave/cli/v2"
)

// ErrNoDependencies is returned when the builder finds no dependencies
var ErrNoDependencies = errors.New("No dependencies")

// NonFatalError indicates that the builder error should not terminate execution
// of the scan. Builders that opt to only enrich dependency scanning instead of
// acting as the SSoT should return a NonFatalError. An example of this would be
// a builder that can refine a scan if connected online. If it's unable to execute
// the build step successfully and defaults to an offline parser, a NonFatalError
// would be a good type for its returned errors.
type NonFatalError struct {
	Msg string
	Err error
}

// NewNonFatalError creates a new NonFatalError and wraps err if not nil.
func NewNonFatalError(msg string, err error) NonFatalError {
	return NonFatalError{Msg: msg, Err: err}
}

func (e NonFatalError) Error() string {
	if e.Err != nil {
		return fmt.Sprintf("%v: %v", e.Msg, e.Err)
	}
	return e.Msg
}

func (e NonFatalError) Unwrap() error {
	return e.Err
}

// Builder is implemented by what can provide a generate a lock file or dependency graph for project.
type Builder interface {
	// Build generates an output file for the given input file, and returns its name along with the paths
	// of outputs for any sub-projects discovered during the Build step
	Build(string) (string, []string, error)
}

// Configurable is implemented by builders that can be configured using CLI flags
type Configurable interface {
	Flags() []cli.Flag            // Flags returns the CLI flags that configure the builder
	Configure(*cli.Context) error // Configure configures the builder based on a CLI context
}
