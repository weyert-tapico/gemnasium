package gem

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/cli"

func init() {
	cli.Register("gem", "gem/vrange.rb")
}
