package python

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

func init() {
	vrange.Register("python", &resolver{})
}

type resolver struct {
	dir string
}

func (r *resolver) Resolve(queries []vrange.Query) (*vrange.ResultSet, error) {
	tmpfile, err := os.CreateTemp("/tmp", "vrange_queries")
	if err != nil {
		return nil, err
	}
	defer os.Remove(tmpfile.Name())
	if err := json.NewEncoder(tmpfile).Encode(queries); err != nil {
		return nil, err
	}
	if err := tmpfile.Close(); err != nil {
		return nil, err
	}

	rangecheck := filepath.Join(r.dir, "rangecheck.py")
	cmd := exec.Command("pipenv", "run", rangecheck, tmpfile.Name())
	cmd.Dir = r.dir
	out, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), out)
	if err != nil {
		return nil, fmt.Errorf("running python vrange command dir=%q cmd=%q err=%q", cmd.Dir, cmd.String(), err)
	}
	results := []struct {
		Version   string
		Range     string
		Satisfies bool
		Error     string
	}{}
	if err := json.Unmarshal(out, &results); err != nil {
		return nil, err
	}

	set := make(vrange.ResultSet)
	for _, result := range results {
		var err error
		if result.Error != "" {
			err = errors.New(result.Error)
		}
		query := vrange.Query{Version: result.Version, Range: result.Range}
		set.Set(query, result.Satisfies, err)
	}
	return &set, nil
}

func (*resolver) Flags() []cli.Flag {
	return nil
}

func (r *resolver) Configure(c *cli.Context, opts vrange.Options) error {
	r.dir = filepath.Join(opts.BaseDir, "python")
	cmd := exec.Command("pipenv", "sync", "--verbose")
	cmd.Dir = r.dir
	out, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), out)
	if err != nil {
		return fmt.Errorf("configuring python vrange command dir=%q cmd=%q err=%q", cmd.Dir, cmd.String(), err)
	}
	return nil
}
