package finder

import (
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/pathfilter"
)

// Preset is a set of pre-defined PackageManagers for a particular
// detection configuration
type Preset int

const (
	// PresetGemnasium makes the finder detect projects for the gemnasium analyzer
	PresetGemnasium Preset = iota

	// PresetGemnasiumMaven makes makes the finder detect projects for the gemnasium-maven analyzer
	PresetGemnasiumMaven

	// PresetGemnasiumPython makes makes the finder detect projects for the gemnasium-python analyzer
	PresetGemnasiumPython

	// PresetSBOMGolang makes makes the finder detect projects for the golang SBOM generator
	PresetSBOMGolang
)

const (
	flagFIPSMode   = "fips-mode"
	envVarFIPSMode = "FIPS_MODE"

	flagIgnoredDirs   = "ignored-dirs"
	envVarIgnoredDirs = "SEARCH_IGNORED_DIRS"

	flagIgnoreHiddenDirs   = "ignore-hidden-dirs"
	envVarIgnoreHiddenDirs = "SEARCH_IGNORE_HIDDEN_DIRS"

	flagMaxDepth   = "max-depth"
	envVarMaxDepth = "SEARCH_MAX_DEPTH"

	flagRequirementsFile   = "pip-requirements-file"
	envVarRequirementsFile = "PIP_REQUIREMENTS_FILE"
)

var defaultIgnoredDirs = cli.NewStringSlice("node_modules", ".bundle", "vendor", ".git")

// Flags generates CLI flags to configure the finder.
// These flags are a subset of the ones defined in the search package of the common library.
func Flags(p Preset) []cli.Flag {
	flags := []cli.Flag{
		&cli.StringSliceFlag{
			Name:    flagIgnoredDirs,
			EnvVars: []string{envVarIgnoredDirs},
			Usage:   "Directory to be ignored",
			Value:   defaultIgnoredDirs,
		},
		&cli.BoolFlag{
			Name:    flagIgnoreHiddenDirs,
			EnvVars: []string{envVarIgnoreHiddenDirs},
			Usage:   "Ignore hidden directories",
			Value:   true,
		},
		&cli.IntFlag{
			Name:    flagMaxDepth,
			EnvVars: []string{envVarMaxDepth},
			Usage:   "Maximum directory depth, set to -1 to ignore",
			Value:   2,
		},
	}
	switch p {
	case PresetGemnasiumMaven:
		flags = append(flags, &cli.BoolFlag{
			Name:    flagFIPSMode,
			EnvVars: []string{envVarFIPSMode},
			Usage:   "FIPS mode enabled",
			Value:   false,
		})
	case PresetGemnasiumPython:
		flags = append(flags, &cli.StringFlag{
			Name:    flagRequirementsFile,
			EnvVars: []string{envVarRequirementsFile},
			Usage:   "Custom requirements file to use when analyzing project",
			Value:   "",
		})
	}
	flags = append(flags, pathfilter.MakeFlags("DS_")...)
	return flags
}

// NewFinder initializes a new finder for a given preset and CLI context
func NewFinder(c *cli.Context, preset Preset) (*Finder, error) {
	filter, err := pathfilter.NewFilter(c)
	if err != nil {
		return nil, err
	}

	cfg := config{
		Preset:   preset,
		FIPSMode: c.Bool(flagFIPSMode),
		MaxDepth: NullInt{
			Valid: c.Int(flagMaxDepth) != -1,
			Value: c.Int(flagMaxDepth),
		},
		IgnoredDirs:         c.StringSlice(flagIgnoredDirs),
		IgnoreHiddenDirs:    c.Bool(flagIgnoreHiddenDirs),
		PipRequirementsFile: c.String(flagRequirementsFile),
		IsExcludedPath:      filter.IsExcluded,
	}

	return newFinder(cfg), nil
}

// config configures a finder
type config struct {
	Preset              Preset
	FIPSMode            bool
	HasMaxDepth         bool
	MaxDepth            NullInt
	IgnoredDirs         []string
	IgnoreHiddenDirs    bool
	PipRequirementsFile string
	IsExcludedPath      func(string) bool
}

// newFinder initializes a finder using the given configuration.
// It ensures that options are applied in a consistent manner not matter what the preset is.
func newFinder(cfg config) *Finder {
	f := newFinderWithPreset(cfg)
	f.FIPSMode = cfg.FIPSMode
	f.MaxDepth = cfg.MaxDepth
	f.IgnoredDirs = cfg.IgnoredDirs
	f.IgnoreHiddenDirs = cfg.IgnoreHiddenDirs
	f.IsExcludedPath = cfg.IsExcludedPath
	return f
}

// newFinderWithPreset initializes a finder using the preset and preset-specific options.
// Generic options of the configuration are not applied to the new finder.
func newFinderWithPreset(cfg config) *Finder {
	switch cfg.Preset {
	case PresetGemnasium:
		return &Finder{
			Detect: NewDetect(
				PackageManagerConan,
				PackageManagerComposer,
				PackageManagerGo,
				PackageManagerNpm,
				PackageManagerNuget,
				PackageManagerYarn,
				PackageManagerBundler,
			),
			FileTypes:  []FileType{FileTypeLockFile},
			SearchMode: SearchAll,
		}

	case PresetGemnasiumMaven:
		// Supported package managers are Maven, Gradle, and Sbt, in that order.
		// Gradle projects are not detected when FIPS mode is enabled.
		pkgManagers := []PackageManager{PackageManagerMaven}
		if cfg.FIPSMode {
			log.Warn("Gradle is not supported when FIPS mode is enabled")
		} else {
			pkgManagers = append(pkgManagers, PackageManagerGradle)
		}
		pkgManagers = append(pkgManagers, PackageManagerSbt)

		return &Finder{
			Detect:     NewDetect(pkgManagers...),
			FileTypes:  []FileType{FileTypeRequirements},
			SearchMode: SearchSingleDir,
		}

	case PresetGemnasiumPython:
		// prepend PIP_REQUIREMENTS_FILE to pip requirements file if set
		pip := PackageManagerPip
		if filename := cfg.PipRequirementsFile; filename != "" {
			file := File{Filename: filename, FileType: FileTypeRequirements}
			pip.Files = append([]File{file}, pip.Files...)
		}

		return &Finder{
			Detect: NewDetect(
				pip,
				PackageManagerPipenv,
				PackageManagerPoetry,
				// setuptools must be last because pip and pipenv projects
				// might have a setuptools script (setup.py)
				PackageManagerSetuptools,
			),
			FileTypes:  []FileType{FileTypeRequirements},
			SearchMode: SearchSingleDir,
		}

	case PresetSBOMGolang:
		return &Finder{
			Detect: NewDetect(
				PackageManagerGo,
			),
			FileTypes:  []FileType{FileTypeLockFile},
			SearchMode: SearchAll,
		}

	default:
		return &Finder{Detect: NewDetect()}
	}
}
