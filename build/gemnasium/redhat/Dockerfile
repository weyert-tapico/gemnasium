FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.18 AS analyzer-build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go version && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer \
    cmd/gemnasium/main.go

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS vrange-nuget-build

COPY vrange/nuget/Vrange /app
WORKDIR /app

RUN \
	dotnet restore && \
	dotnet build && \
	dotnet publish -c Release -r linux-x64 --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true && \
	cp bin/Release/netcoreapp3.1/linux-x64/publish/Vrange /vrange-linux

FROM registry.access.redhat.com/ubi8-minimal
USER root

ARG RETIRE_JS_VERSION
ENV RETIRE_JS_VERSION ${RETIRE_JS_VERSION:-3.0.7}

ARG RETIREJS_JS_ADVISORY_DB_URL="https://raw.githubusercontent.com/RetireJS/retire.js/master/repository/jsrepository.json"
ARG RETIREJS_JS_ADVISORY_DB="/jsrepository.json"

# See https://getcomposer.org/download/ for available versions of PHP Composer and corresponding checksums.
ARG COMPOSER_VERSION="2.4.1"
ARG COMPOSER_SHA256SUM="ea8cf6308ec76ff9645c3818841a7588096b9dc2767345fbd4bd492dd8a6dca6"
ARG COMPOSER_DOWNLOAD_PATH="/tmp/composer.phar"
ARG COMPOSER_BIN="/usr/local/bin/composer"

# GEMNASIUM_RETIREJS_JS_ADVISORY_DB is used internally
# to communicate the local path of jsrepository.json to libfinder.
# The GEMNASIUM_ prefix prevents collisions with RETIREJS_JS_ADVISORY_DB
# of the retire.js analyzer.
ENV GEMNASIUM_RETIREJS_JS_ADVISORY_DB $RETIREJS_JS_ADVISORY_DB

COPY vrange /vrange
COPY --from=vrange-nuget-build /vrange-linux /vrange/nuget/vrange-linux

ENV VRANGE_DIR="/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

# Necessary because of this behaviour with NodeJS >= 15: https://stackoverflow.com/a/65443098/17139068
WORKDIR /tmp

RUN \
	# ensure we have the most recent versions of all installed packages
	microdnf update --nodocs && \
	# WARNING: The ubi8-minimal images purposefully do not include the timezone
	# data inside of /usr/share/zoneinfo to save space. This causes
	# composer to exit with a fatal error in PHP7, and segfault in PHP8.
	# This is because composer assumes that the timezone database exists.
	# To fix the fatal error / segfault issue, it's required to force a
	# re-install of tzdata and libksba which is also required by composer.
	# See https://bugzilla.redhat.com/show_bug.cgi?id=1903219 for more info.
	microdnf reinstall tzdata libksba && \
	\
	# install required dependencies
    microdnf module enable nodejs:16 && \
    microdnf install nodejs git --nodocs && \
	\
	# gemnasium-db
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# vrange/php dependencies
    microdnf module enable php:8.0 && \
    microdnf install php php-common php-zip php-json php-xml php-mbstring --nodocs && \
	curl -o $COMPOSER_DOWNLOAD_PATH https://getcomposer.org/download/$COMPOSER_VERSION/composer.phar && \
	sha256sum $COMPOSER_DOWNLOAD_PATH | grep $COMPOSER_SHA256SUM && \
	mv $COMPOSER_DOWNLOAD_PATH $COMPOSER_BIN && \
	chmod a+x $COMPOSER_BIN && \
        # update dependencies to latest version
        composer update -d "$VRANGE_DIR/php" && \
        composer install -d "$VRANGE_DIR/php" && \
	\
	# vrange/gem dependencies
    microdnf module enable ruby:3.0 && \
    microdnf install ruby rubygem-json --nodocs && \
	\
	# vrange/npm dependencies
	npm install -g yarn && \
	yarn --cwd "$VRANGE_DIR/npm/" && \
	\
	# vrange/nuget dependencies
    microdnf install gettext-libs --nodocs && \
	\
	# give write access to CA certificates (OpenShift)
	mkdir -p /etc/ssl/certs/ && \
	touch /etc/ssl/certs/ca-certificates.crt && \
	chmod g+w /etc/ssl/certs/ca-certificates.crt && \
        \
	# give write access to vulnerability database (OpenShift)
	chmod -R g+w $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# install retire.js
	npm install -g retire@$RETIRE_JS_VERSION && \
	curl -o $RETIREJS_JS_ADVISORY_DB $RETIREJS_JS_ADVISORY_DB_URL && \
	\
	# download go toolchain required for determining the final list of modules used in a Go project
	# via the golang.org/x/tools/go/packages package.
    microdnf install go --nodocs && \
	# clean microdnf cache
    microdnf clean all && \
	echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"


COPY --from=analyzer-build /analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
