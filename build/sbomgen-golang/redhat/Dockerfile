FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.18 AS sbomgen-build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    go version && \
    go build -ldflags="-X 'main.Version=$CHANGELOG_VERSION'" -o /sbomgen-golang \
    cmd/sbomgen-golang/main.go

FROM registry.access.redhat.com/ubi8/ubi-minimal
USER root

# download go toolchain required for determining the final list of modules used in a Go project
# via the golang.org/x/tools/go/packages package.
RUN microdnf install go --nodocs && \
	microdnf clean all && \
	# give write access to CA certificates (OpenShift)
	mkdir -p /etc/ssl/certs/ && \
	touch /etc/ssl/certs/ca-certificates.crt && \
	chmod g+w /etc/ssl/certs/ca-certificates.crt && \
    \
	echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

COPY --from=sbomgen-build /sbomgen-golang /usr/local/bin/sbomgen-golang

ENTRYPOINT []
CMD ["sbomgen-golang", "run"]
