FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.18 AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app

COPY go.mod go.sum ./
RUN go mod download

COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go version && \
    go build -ldflags="-X '$PATH_TO_MODULE/cmd/gemnasium-python/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer \
    cmd/gemnasium-python/main.go

# A generic ubi8 image is used instead of ubi8/python-39
# because "python -m venv" is executed when building ubi8/python-39,
# and venv is not compatible with pip user installs (just like virtualenv).
# See https://pip.pypa.io/en/stable/user_guide/#user-installs
FROM registry.access.redhat.com/ubi8-minimal
USER root

ARG DS_PYTHON_VERSION=3.9
ENV DS_PYTHON_VERSION $DS_PYTHON_VERSION

COPY vrange/python /vrange/python
ENV VRANGE_DIR="/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ARG DS_GET_PIP_PATH="/get-pip.py"
ENV DS_GET_PIP_PATH $DS_GET_PIP_PATH

ARG PIPENV_DEFAULT_PYTHON_VERSION="3"
ENV PIPENV_DEFAULT_PYTHON_VERSION $PIPENV_DEFAULT_PYTHON_VERSION

ADD build/gemnasium-python/get-pip.py build/gemnasium-python/requirements.txt /

RUN microdnf update --nodocs && \
	PYTHON_PKG=python${DS_PYTHON_VERSION/./} && \
	\
	# install git, python, pip, and packages needed to build Python packages
	microdnf install git $PYTHON_PKG $PYTHON_PKG-pip $PYTHON_PKG-devel gcc-gfortran && \
	\
	# install pipenv and its requirements
	pip3 install -r /requirements.txt && \
	\
	# install vrange/python
	DIR=${PWD} && cd ${VRANGE_DIR}/python && \
	pipenv sync --verbose && cd ${DIR} && \
	\
	# clone gemnasium-db
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# give write access to CA certificates (OpenShift)
	mkdir -p /etc/ssl/certs/ && \
        touch /etc/ssl/certs/ca-certificates.crt && \
	chmod g+w /etc/ssl/certs/ca-certificates.crt && \
	\
	# give write access to vulnerability database (OpenShift)
	chmod -R g+w $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# clean microdnf cache
    microdnf clean all && \
	echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer", "run"]
