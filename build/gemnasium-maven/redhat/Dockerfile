FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.18 AS analyzer-builder
# Build a static binary
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go version && \
    go build -ldflags="-X '$PATH_TO_MODULE/cmd/gemnasium-maven/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer \
    cmd/gemnasium-maven/main.go

FROM registry.access.redhat.com/ubi8-minimal

RUN microdnf update --nodocs

ENV VRANGE_DIR="/vrange"
ARG GEMNASIUM_VRANGE_BRANCH="v2.3.0"
ARG GEMNASIUM_VRANGE_REPO="https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium"
ARG GEMNASIUM_VRANGE_URL="$GEMNASIUM_VRANGE_REPO/raw/$GEMNASIUM_VRANGE_BRANCH/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ADD build/gemnasium-maven/utils/maven-plugin-builder /maven-plugin-builder
ADD build/gemnasium-maven/gemnasium-gradle-plugin-init.gradle /
ENV GRADLE_PLUGIN_INIT_PATH="/gemnasium-gradle-plugin-init.gradle"
ADD build/gemnasium-maven/utils/gradle-plugin-builder /gradle-plugin-builder

ADD build/gemnasium-maven/utils/sbt-dependency-graph-plugin /sbt-dependency-graph-plugin
ENV DEP_GRAPH_PLUGIN="addSbtPlugin(\"net.virtual-void\" % \"sbt-dependency-graph\" % \"0.10.0-RC1\" from \"file:///sbt-dependency-graph-plugin/sbt-dependency-graph-0.10.0-RC1.jar\")"

# Disable features that are incompatible with FIPS mode
ENV FIPS_MODE="true"

ENV ASDF_DATA_DIR="/opt/asdf"
ENV ASDF_VERSION="v0.8.1"
ENV HOME=/gemnasium-maven
ENV TERM="xterm"
WORKDIR $HOME
COPY build/gemnasium-maven/redhat/install.sh /root
COPY build/gemnasium-maven/redhat/config/.tool-versions $HOME
COPY build/gemnasium-maven/redhat/config/.bashrc $HOME
RUN cat /etc/*-release; \
        bash /root/install.sh

# Install analyzer
COPY --from=analyzer-builder --chown=root:root /go/src/app/analyzer /analyzer-binary
COPY build/gemnasium-maven/analyzer-wrapper /analyzer

CMD ["/analyzer", "run"]
