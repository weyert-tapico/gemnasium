require "tmpdir"
require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/sbom_manifest_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe "running image" do
  let(:fixtures_dir) { "qa/fixtures" }
  let(:expectations_dir) { "qa/expect" }

  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context "with test project" do
    def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
      path = File.join(expectations_dir, expectation_name, report_filename)
      JSON.parse(File.read path)
    end

    let(:global_vars) do
      { "GEMNASIUM_DB_REF_NAME": "v1.2.142" }
    end

    let(:project) { "any" }
    let(:relative_expectation_dir) { project }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    let(:report) { scan.report }

    context "containing multiple sub-projects with subdirs" do
      let(:project) { "multi-project/subdirs" }

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"
        # TODO: allow separating vulnerability and dependencies from the following shared behaviour, so that
        # we can create fixture files that only have a dependency without a related vulnerability
        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like "valid report"

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) {
            ["go-project/gl-sbom-go-go.cdx.json", "javascript-project/gl-sbom-npm-npm.cdx.json",
             "ruby-project-1/gl-sbom-gem-bundler.cdx.json", "ruby-project-2/gl-sbom-gem-bundler.cdx.json"]
          }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end

        describe "SBOM Manifest" do
          let(:relative_sbom_manifest_path) { "sbom-manifest.json" }

          it_behaves_like "non-empty SBOM manifest"
          it_behaves_like "recorded SBOM manifest"
        end
      end
    end

    context "containing multiple sub-projects" do
      let(:project) { "multi-project/default" }

      it_behaves_like "successful scan" do
        # See for more info: https://gitlab.com/gitlab-org/gitlab/-/issues/370613
        it "mentions issue tracking composer dev package parsing activation" do
          expect(scan.combined_output).to match(/issues\/370613/)
        end
      end

      describe "created report" do
        it_behaves_like "non-empty report"
        it_behaves_like "valid report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) {
            ["gl-sbom-go-go.cdx.json", "gl-sbom-gem-bundler.cdx.json", "gl-sbom-packagist-composer.cdx.json"]
          }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end
    end

    context "with csharp-nuget-dotnetcore" do
      let(:project) { "csharp-nuget-dotnetcore/default" }

      context "when all dependencies have a dependency path" do
        let(:variables) do
          { DS_DEPENDENCY_PATH_MODE: "all" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["src/web.api/gl-sbom-nuget-nuget.cdx.json"] }

          it_behaves_like "expected CycloneDX metadata tool-name", "Gemnasium"

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end
    end

    context "with c-conan" do
      let(:project) { "c-conan/default" }

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like "valid report"
      end

      describe "CycloneDX SBOMs" do
        let(:relative_sbom_paths) { ["gl-sbom-conan-conan.cdx.json"] }

        it_behaves_like "non-empty CycloneDX files"
        it_behaves_like "recorded CycloneDX files"
        it_behaves_like "valid CycloneDX files"
      end

      # TODO: Remove or re-add tests if schema model 15 support is re-released
      # context "when using schema model 15" do
      #   let(:variables) do
      #     { DS_SCHEMA_MODEL: "15" }
      #   end

      #   it_behaves_like "successful scan"

      #   describe "created report" do
      #     it_behaves_like "non-empty report"

      #     it_behaves_like "recorded report" do
      #       let(:recorded_report) { parse_expected_report("c-conan/model-15") }
      #     end

      #     it_behaves_like "valid report"
      #   end
      # end
    end

    context "with go-modules" do
      context "when parsing go-project-modules.json" do
        let(:project) { "go-modules/gomod/default" }

        context "when using the default config" do
          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "valid report"
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(project) }
            end
          end
        end

        context "when building for windows/amd64" do
          # See https://go.dev/doc/install/source#environment for a full list of valid values.
          # Logrus v1.4.2 imports github.com/konsorten/windows-terminal-sequences only within
          # windows builds: https://github.com/sirupsen/logrus/blob/v1.4.2/terminal_check_windows.go
          let(:variables) do
            {
              GOOS: "windows",
              GOARCH: "amd64",
            }
          end

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "valid report"
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("go-modules/gomod/windows-amd64-build") }
            end
          end
        end

        context "when adding build tags" do
          let(:variables) { { GOFLAGS: "-tags=experimental" } }

          it_behaves_like "successful scan"

          describe "created_report" do
            it_behaves_like "valid report"
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("go-modules/gomod/build-tags") }
            end
          end
        end

        context "when non-fatal error encountered" do
          let(:script) do
            <<~HERE
            #!/bin/sh

            # Set the HOME directory to /dev/null so that go
            # build errors out.
            export HOME=/dev/null

            # Run the analyzer
            /analyzer run
            HERE
          end

          context "when using default config" do
            it "logs non-fatal error with WARN level" do
              expect(scan.combined_output).to match(/[WARN].+Non-fatal error encountered while building project/)
            end

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "valid report"
              it_behaves_like "non-empty report"
            end
          end

          context "when excluding go.sum with DS_EXCLUDED_PATHS" do
            let(:variables) { { DS_EXCLUDED_PATHS: "/go.sum" } }

            it "logs non-fatal error with WARN level" do
              expect(scan.combined_output).to match(/[WARN].+Non-fatal error encountered while building project/)
            end

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "valid report"
              it_behaves_like "empty report"
            end
          end
        end

        context "when in a subdirectory" do
          let(:project) { "go-modules/gomod/subdir" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"
            it_behaves_like "valid report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(project) }
            end
          end
        end
      end

      context "when parsing go.sum" do
        let(:project) { "go-modules/gosum/default" }
        context "when using the default config" do
          it_behaves_like "successful scan"
    
          it "selects the go.sum parser and warns that false positives may occur" do
            expect(scan.combined_output).to match(/Selecting "go.sum" parser for "[^"]+"/)
            expect(scan.combined_output).to match(/False positives may occur/)
          end

          describe "created report" do
            it_behaves_like "valid report"
            it_behaves_like "non-empty report"
    
            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(project) }
            end
          end
        end

        context "using sbom command" do
          let(:script) do
            <<-HERE
#!/bin/sh

/analyzer sbom
            HERE
          end

          context "when using the default config" do
            it "does not create a report" do
              expect(File).not_to exist(scan.report_path)
            end

            describe "CycloneDX SBOMs" do
              let(:relative_sbom_paths) { ["gl-sbom-go-go.cdx.json"] }

              it_behaves_like "non-empty CycloneDX files"
              it_behaves_like "recorded CycloneDX files"
              it_behaves_like "valid CycloneDX files"
            end
          end

          context "and setting ADDITIONAL_CA_CERT_BUNDLE" do
            let(:variables) do
              { 
                ADDITIONAL_CA_CERT_BUNDLE: "testing",
              }
            end

            let(:script) do
              <<~HERE
              #!/bin/sh

              /analyzer run
              tail -n 1 /etc/ssl/certs/ca-certificates.crt
              HERE
            end

            it "appends the ADDITIONAL_CA_CERT_BUNDLE variable content to /etc/ssl/certs/ca-certificates.crt" do
              expect(scan.combined_output).to match(variables[:ADDITIONAL_CA_CERT_BUNDLE])
            end

            describe "CycloneDX SBOMs" do
              let(:relative_sbom_paths) { ["gl-sbom-go-go.cdx.json"] }

              it_behaves_like "non-empty CycloneDX files"
              it_behaves_like "recorded CycloneDX files"
              it_behaves_like "valid CycloneDX files"
            end
          end
        end

        context "when offline" do
          let(:offline) { true }

          context "when gemnasium-db update disabled" do
            let(:variables) do
              { GEMNASIUM_DB_UPDATE_DISABLED: "true" }
            end

            it_behaves_like "successful scan"
          end

          context "when gemnasium-db update NOT disabled" do
            it_behaves_like "crashed scan"
          end
        end

        context "with clone of gemnasium-db" do
          let(:project) { "go-modules/gosum/advisory-db-scan-time-sync" }

          let(:gemnasium_db_ref_name) { "master" }

          let(:clone_config) do
            # config for setup_local_remote.sh
            {
              ORIGINAL_REMOTE_STABLE_BRANCH: "2020-03-13",
              ORIGINAL_REMOTE_OLDER_STABLE_BRANCH: "2020-01-15",
              NEW_BRANCH_NAME: "new-branch-foobarbaz",
              NEW_TAG_NAME: "new-tag-foobarbaz"
            }
          end

          let(:variables) do
            {
              GEMNASIUM_DB_REF_NAME: gemnasium_db_ref_name,
              GEMNASIUM_DB_REMOTE_URL: "/gemnasium-db-local-remote"
            }.merge(clone_config)
          end

          let(:script) do
            setup_script_path = File.expand_path("../qa/scripts/setup_local_remote.sh", __dir__)
            setup_script = File.read(setup_script_path)
            <<-HERE
#!/bin/sh
set -ex
git config --global user.email "you@example.com"

# setup_local_remote.sh
#{setup_script}

/analyzer run
            HERE
          end

          describe "when requested git ref is a branch that has been updated remotely" do
            let(:gemnasium_db_ref_name) { clone_config[:ORIGINAL_REMOTE_STABLE_BRANCH] }

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "recorded report" do
                let(:recorded_report) do
                  parse_expected_report(project, report_filename: "branch-updated-report.json")
                end
              end
            end
          end

          describe "when requested git ref is a branch that has NOT been updated remotely" do
            let(:gemnasium_db_ref_name) { clone_config[:ORIGINAL_REMOTE_OLDER_STABLE_BRANCH] }

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "recorded report" do
                let(:recorded_report) do
                  parse_expected_report(project, report_filename: "old-branch-checkout-report.json")
                end
              end
            end
          end

          describe "when requested git ref is a branch that only exists remotely" do
            let(:gemnasium_db_ref_name) { clone_config[:NEW_BRANCH_NAME] }

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "recorded report" do
                let(:recorded_report) do
                  parse_expected_report(project, report_filename: "new-branch-checkout-report.json")
                end
              end
            end
          end

          describe "when requested git ref is a tag that exists locally" do
            let(:gemnasium_db_ref_name) { "v1.0.199" }

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "recorded report" do
                let(:recorded_report) do
                  parse_expected_report(project, report_filename: "old-tag-checkout-report.json")
                end
              end
            end
          end

          describe "when requested git ref is a tag that only exists remotely" do
            let(:gemnasium_db_ref_name) { clone_config[:NEW_TAG_NAME] }

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "recorded report" do
                let(:recorded_report) do
                  parse_expected_report(project, report_filename: "new-tag-checkout-report.json")
                end
              end
            end
          end

          describe "when requested git ref is a commit SHA" do
            let(:gemnasium_db_ref_name) { "26c90d4a2dd9901bd9c18fc809f9c692f56e2979" }

            it_behaves_like "successful scan"

            describe "created report" do
              it_behaves_like "recorded report" do
                let(:recorded_report) do
                  parse_expected_report(project, report_filename: "checkout-by-commit-sha-report.json")
                end
              end
            end
          end
        end

        context "when in a subdirectory" do
          let(:project) { "go-modules/gosum/subdir" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"
            it_behaves_like "valid report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(project) }
            end
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["subdir/gl-sbom-go-go.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end
      end
    end

    context "with js-npm" do
      context "with lockfile v1" do
        let(:project) { "js-npm/default" }
        context "with include dev dependencies enabled" do
          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(project) }
            end

            it_behaves_like "valid report"
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-npm-npm.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end

        context "with include dev dependencies disabled" do
          let(:variables) do
            { "DS_INCLUDE_DEV_DEPENDENCIES": "false" }
          end

          it_behaves_like "successful scan"
          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("js-npm/lockfile-v1-ignore-dev-dependencies") }
            end

            it_behaves_like "valid report"
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-npm-npm.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            # TODO: re-enable after updating integration-test project to accept expected_sbom_paths
            # it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end
      end

      context "with minified jquery" do
        let(:project) { "js-npm/minified-jquery" }

        context "when not enabling scan of libraries" do
          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("js-npm/default") }
            end

            it_behaves_like "valid report"
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-npm-npm.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end

        context "when enabling scan of libraries" do
          let(:variables) { { "GEMNASIUM_LIBRARY_SCAN_ENABLED": "true" } }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(project) }
            end

            it_behaves_like "valid report"
          end

          # TODO: this check is in place to confirm that the bug in https://gitlab.com/gitlab-org/gitlab/-/issues/360799
          #       has been addressed. This check should be removed when working on https://gitlab.com/gitlab-org/gitlab/-/issues/361604.
          it "does not create CycloneDX files for library packages" do
            expect(File).not_to exist(File.join(scan.target_dir, "node_modules/jed/test/gl-sbom-npm-.cdx.json"))
          end
        end

        context "when enabling scan of libraries but retire fails" do
          let(:variables) {
            {
              "GEMNASIUM_LIBRARY_SCAN_ENABLED": "true",
              "GEMNASIUM_RETIREJS_JS_ADVISORY_DB": "/missing",
            }
          }

          it_behaves_like "failed scan"

          it "logs why retire has failed" do
            expect(scan.combined_output).to match /no such file or directory/
          end
        end

        context "when enabling scan of libs but excluding JSONSelect" do
          let(:variables) {
            {
              "GEMNASIUM_LIBRARY_SCAN_ENABLED": "true",
              "DS_EXCLUDED_PATHS": "JSONSelect",
            }
          }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            describe "vulnerable files" do
              specify do
                skip "report is missing" if report.nil?

                vuln_files = report["vulnerabilities"].map { |v| v.dig("location", "file") }.uniq.sort
                expect(vuln_files).to eql [
                  "node_modules/await-to-js/dist/docs/assets/js/main.js",
                  "node_modules/jed/test/jquery.min.js",
                  "package-lock.json"
                ].sort
              end
            end
          end
        end
      end

      context "with lockfile versions 2 or 3" do
        let(:project) { "js-npm/lockfileVersion2" }

        context "with include dev dependencies enabled" do
          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("js-npm/default") }
            end

            it_behaves_like "valid report"
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-npm-npm.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end

        context "with include dev dependencies disabled" do
          let(:variables) do
            { "DS_INCLUDE_DEV_DEPENDENCIES": "false" }
          end

          it_behaves_like "successful scan"
          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("js-npm/lockfile-v2-ignore-dev-dependencies") }
            end

            it_behaves_like "valid report"
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-npm-npm.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            # TODO: re-enable after updating integration-test project to accept expected_sbom_paths
            # it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end
      end

      context "with shrinkwrap file" do
        let(:project) { "js-npm/shrinkwrap" }

        context "with include dev dependencies enabled" do
          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(project) }
            end

            it_behaves_like "valid report"
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-npm-npm.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end

        context "with include dev dependencies disabled" do
          let(:variables) do
            { "DS_INCLUDE_DEV_DEPENDENCIES": "false" }
          end

          it_behaves_like "successful scan"
          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("js-npm/shrinkwrap-ignore-dev-dependencies") }
            end

            it_behaves_like "valid report"
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-npm-npm.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            # TODO: re-enable after updating integration-test project to accept expected_sbom_paths
            # it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end
      end
    end

    context "with js-yarn" do
      context "when remediation is disabled" do
        let(:project) { "js-yarn/default" }

        let(:variables) do
          { DS_REMEDIATE: "false" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-npm-yarn.cdx.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "when remediation is enabled" do
        let(:project) { "js-yarn/remediate-top-level" }

        let(:variables) do
          { DS_REMEDIATE: "true" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"

          # NOTE: The generated report does not contain the expected remediations
          # because auto-remediation requires a valid git directory.
          # TODO: fix with https://gitlab.com/gitlab-org/gitlab/-/issues/370486
          pending "recorded report"

          it_behaves_like "valid report"
        end
      end
    end

    context "with php-composer" do
      let(:project) { "php-composer/default" }

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like "valid report"
      end

      describe "CycloneDX SBOMs" do
        let(:relative_sbom_paths) { ["gl-sbom-packagist-composer.cdx.json"] }

        it_behaves_like "non-empty CycloneDX files"
        it_behaves_like "recorded CycloneDX files"
        it_behaves_like "valid CycloneDX files"
      end

      context "with broken composer.lock" do
        let(:project) { "php-composer/broken" }

        it_behaves_like "failed scan"

        context "but the broken file is excluded" do
          let(:variables) { { "DS_EXCLUDED_PATHS": "composer.lock" } }

          it_behaves_like "successful scan"
        end
      end

      context "when dev dependencies are excluded" do
        let(:project) { "php-composer/default" }
        let(:variables) { { "DS_INCLUDE_DEV_DEPENDENCIES": "false" }}

        it_behaves_like "successful scan" do
          # See for more info: https://gitlab.com/gitlab-org/gitlab/-/issues/370613
          it "mentions issue tracking composer dev package parsing activation" do
            expect(scan.combined_output).to match(/issues\/370613/)
          end
        end

        describe "created report" do
          it_behaves_like "non-empty report"

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report("php-composer/default") }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-packagist-composer.cdx.json"] }
          let(:relative_expectation_dir) { "php-composer/default" }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end
    end

    context "with ruby-bundler" do
      context "with Gemfile and Gemfile.lock" do
        let(:project) { "ruby-bundler/default" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-gem-bundler.cdx.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end

        context "with a vulnerability database where advisories have multiple identifiers" do
          let(:variables) do
            # switch to a version of the vulnerability database where some advisories
            # have multiple identifiers in the "identifiers" YAML field;
            # see CVE-2021-41098 (nokogiri) for instance
            { "GEMNASIUM_DB_REF_NAME": "v2.0.481" }
          end

          describe "created report" do
            it_behaves_like "non-empty report"

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report("ruby-bundler/gemnasium-db-v2.0.481") }
            end

            it_behaves_like "valid report"
          end
        end
      end

      context "when gems.rb and gems.locked" do
        let(:project) { "ruby-bundler/with-gems.locked" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "valid report"

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-gem-bundler.cdx.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end
    end
  end
end
