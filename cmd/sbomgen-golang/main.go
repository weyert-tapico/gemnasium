package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cli/flags"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/sbomscanner"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/golang"
)

const (
	// vendor is the maintainer of the CLI, and the vendor of the CycloneDX tool
	vendor = "GitLab"

	// the language this SBOM generator tool will produce SBOMs for
	language = "golang"
)

var (
	// Version is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	Version = "not-configured"

	// name is the name of the CLI, and the name of the CycloneDX tool
	name = fmt.Sprintf("sbomgen-%s", language)
)

func main() {
	app := cli.NewApp()
	app.Name = name
	app.Version = Version
	app.Authors = []*cli.Author{{Name: vendor}}
	app.Usage = fmt.Sprintf("%s SBOM generator for %s v%s", vendor, language, Version)

	log.SetFormatter(&logutil.Formatter{Project: name})
	log.Info(app.Usage)

	app.Commands = []*cli.Command{
		runCommand(runFlags(), finder.PresetSBOMGolang),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func runFlags() []cli.Flag {
	// TODO: add support for flagScanLibs as part of https://gitlab.com/gitlab-org/gitlab/-/issues/361604
	return flags.New(
		cacert.NewFlags(),
		finder.Flags(finder.PresetSBOMGolang),
	)
}

func runCommand(cliflags []cli.Flag, finderPreset finder.Preset) *cli.Command {
	return &cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the sbom-generator on detected project and generate a compatible SBOM",
		Flags:   cliflags,
		Action: func(c *cli.Context) error {
			startTime := time.Now()

			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// configure builders
			if err := builder.Configure(c); err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flags.TargetDir))
			if err != nil {
				return err
			}

			// find supported projects
			find, err := finder.NewFinder(c, finderPreset)
			if err != nil {
				return err
			}
			projects, err := find.FindProjects(targetDir)
			if err != nil {
				return err
			}

			// raise warning when there is nothing to scan
			if len(projects) == 0 {
				// mimic search.ErrNotFound error of common/search
				log.Warnf("No match in %s", targetDir)
				return nil
			}

			if err := buildProjects(find.IsExcludedPath, &projects, targetDir); err != nil {
				return err
			}

			// scan target directory
			scanner, err := sbomscanner.NewScanner(c)
			if err != nil {
				return err
			}
			result, err := scanner.ScanProjects(targetDir, projects)
			if err != nil {
				return err
			}

			toolInfo := cyclonedx.ToolInfo{
				Name:    name,
				Vendor:  vendor,
				Version: Version,
			}

			// generate SBOMs and manifest
			cycloneDXSBOMs := cyclonedx.ToSBOMs(result, &startTime, toolInfo)
			return cyclonedx.OutputSBOMs(targetDir, cycloneDXSBOMs)
		},
	}
}

func buildProjects(isExcludedPathFn finder.IsExcludedPathFunc, projects *[]finder.Project, targetDir string) error {
	var nonFatalError builder.NonFatalError
	for i, p := range *projects {
		reqFile, found := p.RequirementsFile()
		if !found {
			continue
		}
		inputPath := filepath.Join(targetDir, p.FilePath(reqFile))
		log.Debugf("Exporting dependencies for %s", inputPath)
		pkgManager := p.PackageManager.Name
		b := builder.Lookup(pkgManager)
		if b == nil {
			log.Warnf("No builder found for package manager %s", pkgManager)
			continue
		}
		outputPath, _, err := b.Build(inputPath)
		if err != nil && !errors.As(err, &nonFatalError) {
			return err
		}

		// Do not add the scannable file if a non fatal error occurred while
		// building the project.
		if err != nil && errors.As(err, &nonFatalError) {
			log.Warnf("Non-fatal error encountered while building project: %v", err)
			continue
		}
		if err != nil && !errors.As(err, &nonFatalError) {
			return err
		}
		(*projects)[i].AddScannableFilename(filepath.Base(outputPath))
	}

	return nil
}
