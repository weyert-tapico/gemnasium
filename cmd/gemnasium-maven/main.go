package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cli/flags"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cli/sbom"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cmd/gemnasium-maven/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/keystore"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/manifest"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"

	// vrange
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/semver"

	// parser plugins
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/sbt"

	// builder plugins
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder/gradle"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder/maven"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder/sbt"
)

var (
	errNoInputFile = errors.New("no supported file")
	errNoBuilder   = errors.New("no builder for requirements file")
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = []*cli.Command{
		findCommand(),
		runCommand(),
		sbom.Command(sbomFlags(), finder.PresetGemnasiumMaven, buildProjects),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func findCommand() *cli.Command {
	return &cli.Command{
		Name:      "find",
		Aliases:   []string{"f"},
		Usage:     "Find compatible files in a directory",
		ArgsUsage: "[directory]",
		Flags:     finder.Flags(finder.PresetGemnasiumMaven),
		Action: func(c *cli.Context) error {
			// one argument is expected
			if c.Args().Len() != 1 {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}
			dir := c.Args().Get(0)

			// find compatible files
			finder, err := finder.NewFinder(c, finder.PresetGemnasiumMaven)
			if err != nil {
				return err
			}
			projects, err := finder.FindProjects(dir)
			if err != nil {
				return err
			}
			for _, project := range projects {
				for _, file := range project.Files {
					fmt.Println(project.FilePath(file))
				}
			}

			return nil
		},
	}
}

func sbomFlags() []cli.Flag {
	return flags.New(
		cacert.NewFlags(),
		finder.Flags(finder.PresetGemnasiumMaven),
		builder.Flags(),
		scanner.Flags(),
		vrange.Flags(),
	)
}

func runFlags() []cli.Flag {
	return sbomFlags()
}

func runCommand() *cli.Command {
	return &cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   runFlags(),
		Action: func(c *cli.Context) error {
			startTime := time.Now()

			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// configure version range resolvers
			if err := vrange.Configure(c); err != nil {
				return err
			}

			// configure CA certificates
			if err := keystore.Update(c); err != nil {
				return err
			}

			// configure builders
			if err := builder.Configure(c); err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flags.TargetDir))
			if err != nil {
				return err
			}

			// find supported projects
			find, err := finder.NewFinder(c, finder.PresetGemnasiumMaven)
			if err != nil {
				return err
			}
			projects, err := find.FindProjects(targetDir)
			if err != nil {
				return err
			}

			// raise warning when there is nothing to scan
			if len(projects) == 0 {
				// mimic search.ErrNotFound error of common/search
				log.Warnf("No match in %s", targetDir)
				return nil
			}

			if err := buildProjects(find.IsExcludedPath, &projects, targetDir); err != nil {
				return err
			}

			// scan target directory
			scanner, err := scanner.NewScanner(c)
			if err != nil {
				return err
			}
			result, err := scanner.ScanProjects(targetDir, projects)
			if err != nil {
				return err
			}

			// convert to Dependency Scanning report
			convertCfg := convert.Config{
				AnalyzerDetails: metadata.ReportAnalyzer,
				ScannerDetails:  metadata.ReportScanner,
				StartTime:       &startTime,
			}
			vulnReport := convert.NewConverter(c, convertCfg).ToReport(result)
			vulnReport.Sort()

			// generate SBOMs and manifest
			cycloneDXSBOMs := cyclonedx.ToSBOMs(result, &startTime, metadata.ToolInfo)
			if err := cyclonedx.OutputSBOMs(c.String(flags.TargetDir), cycloneDXSBOMs); err != nil {
				return err
			}

			if err := manifest.Create(c.String(flags.ArtifactDir), cycloneDXSBOMs, metadata.ReportScanner); err != nil {
				return err
			}

			// write Dependency Scanning report
			artifactPath := filepath.Join(c.String(flags.ArtifactDir), command.ArtifactNameDependencyScanning)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(vulnReport)
		},
	}
}

func buildProjects(isExcludedPathFn finder.IsExcludedPathFunc, projects *[]finder.Project, targetDir string) error {
	// build root projects and their sub-projects, if any
	// NOTE: only one root project is expected, but this is a loop for consistency
	// with the main gemnasium project; see https://gitlab.com/gitlab-org/gitlab/-/issues/292952
	subprojects := []finder.Project{}
	for i, p := range *projects {
		// calculate path to build file
		reqFile, found := p.RequirementsFile()
		if !found {
			continue
		}
		inputPath := filepath.Join(targetDir, p.FilePath(reqFile))
		log.Debugf("Exporting dependencies for %s", inputPath)

		// find builder
		pkgManager := p.PackageManager.Name
		b := builder.Lookup(pkgManager)
		if b == nil {
			log.Errorf("No builder for package manager %s", pkgManager)
			return errNoBuilder
		}

		// build root project and its sub-projects, if any
		rootExportPath, childExportPaths, err := b.Build(inputPath)
		switch err {
		case nil:
			// root project has dependencies listed in root export
			rootExportFilename := filepath.Base(rootExportPath)
			(*projects)[i].AddScannableFilename(rootExportFilename)
		case builder.ErrNoDependencies:
			// root project has no dependencies
		default:
			// error not handled
			return err
		}

		// collect sub-projects
		//
		// NOTE: A project must have a requirement file so that its dependencies and vulnerabilities
		// can be listed in the generated reports. However, right now it's impossible to track
		// build files of subprojects, and subprojects might not even have build files anyways.
		// To work around this, we create a subproject with a build file
		// whose filename matches the one of the parent build file.
		//
		// TODO: update gemnasium/scanner to accept an alias for finder.File
		// See https://gitlab.com/gitlab-org/gitlab/-/issues/336131
		//
		for _, absExportPath := range childExportPaths {
			log.Debugf("Found dependency export for sub-project in %s", absExportPath)

			// get export path relative to scanned directory
			exportPath, err := filepath.Rel(targetDir, absExportPath)
			if err != nil {
				return err
			}
			subdir, exportFilename := filepath.Split(exportPath)

			// skip subproject if relative export path is excluded
			if isExcludedPathFn(exportPath) {
				log.Debugf("skip sub-project because export file is excluded: %s", exportPath)
				continue
			}

			// skip subproject if relative build file is excluded
			buildFilePath := filepath.Join(subdir, reqFile.Filename)
			if isExcludedPathFn(buildFilePath) {
				log.Debugf("skip sub-project because build file is excluded: %s", buildFilePath)
				continue
			}

			// create subproject with scannable graph export and fake build file
			subproject := finder.Project{
				Dir: subdir,
				Files: []finder.File{
					{
						Filename: reqFile.Filename,
						FileType: finder.FileTypeRequirements,
					},
					{
						Filename: exportFilename,
						FileType: finder.FileTypeGraphExport,
					},
				},
				PackageManager: (*projects)[i].PackageManager,
			}

			subprojects = append(subprojects, subproject)
		}
	}

	*projects = append(*projects, subprojects...)
	return nil
}
