//go:build experimental

package main

import (
	"time"

	"go.uber.org/zap"
)

func Log() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	sugar := logger.Sugar()
	sugar.Infof("logger=zap current_time=%s", time.Now().String())
}
