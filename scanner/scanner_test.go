package scanner

import (
	"encoding/json"
	"flag"
	"os"
	"path/filepath"
	"runtime"
	"sort"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	vrange "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/cli"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/golang"
)

func init() {
	// register version range resolvers
	for _, syntax := range []string{"npm", "gem", "python"} {
		vrange.Register(syntax, "../vrange/semver/vrange-"+runtime.GOOS, "npm")
	}
}

func TestScanner(t *testing.T) {
	fixturesDir := "testdata/depfiles"

	// scanner
	set := flag.NewFlagSet("test", 0)
	set.String("gemnasium-db-local-path", "testdata/gemnasium-db", "doc")
	set.Bool("gemnasium-db-update-disabled", true, "doc")
	c := cli.NewContext(nil, set, nil)
	scanner, err := NewScanner(c)
	require.NoError(t, err)

	t.Run("ScanProjects", func(t *testing.T) {
		projects := []finder.Project{
			{
				Dir: "pypi",
				Files: []finder.File{
					{
						Filename: "pipdeptree.json",
						FileType: finder.FileTypeGraphExport,
					},
				},
				PackageManager: finder.PackageManagerPip,
			},
			{
				Dir: "go",
				Files: []finder.File{
					{
						Filename: "go.sum",
						FileType: finder.FileTypeLockFile,
					},
				},
				PackageManager: finder.PackageManagerGo,
			},
			{
				Dir: "yarn",
				Files: []finder.File{
					{
						Filename: "yarn.lock",
						FileType: finder.FileTypeLockFile,
					},
				},
				PackageManager: finder.PackageManagerYarn,
			},
			{
				Dir: "bundler",
				Files: []finder.File{
					{
						Filename: "Gemfile.lock",
						FileType: finder.FileTypeLockFile,
					},
				},
				PackageManager: finder.PackageManagerBundler,
			},
		}
		got, err := scanner.ScanProjects(fixturesDir, projects)
		require.NoError(t, err)

		// ignore dependencies because pointers cannot be compared
		for i := range got {
			got[i].Dependencies = nil
		}

		want := []File{}
		sort.Slice(got, func(i, j int) bool {
			return got[j].Path > got[i].Path
		})
		checkExpectedFile(t, &want, &got, filepath.Join(fixturesDir, "scandir.json"))
	})

	t.Run("scanFile", func(t *testing.T) {
		t.Run("Gemfile.lock", func(t *testing.T) {
			relPath, alias := "bundler/Gemfile.lock", "bundler/Gemfile"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: alias, PackageManager: "bundler"}

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-gem.json"))
		})

		t.Run("pipdeptree.json", func(t *testing.T) {
			relPath := "pypi/pipdeptree.json"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: relPath, PackageManager: "pip"}

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-pypi.json"))
		})

		t.Run("go.sum", func(t *testing.T) {
			relPath := "go/go.sum"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: relPath, PackageManager: "go"}

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-go.json"))
		})
	})
}

func TestScanner_GemOnlyRepo(t *testing.T) {
	fixturesDir := "testdata/depfiles"

	// scanner
	set := flag.NewFlagSet("test", 0)
	set.String("gemnasium-db-local-path", "testdata/gemnasium-db-gem", "doc")
	set.Bool("gemnasium-db-update-disabled", true, "doc")
	c := cli.NewContext(nil, set, nil)
	scanner, err := NewScanner(c)
	require.NoError(t, err)

	var tcs = []struct {
		file string
		want error
	}{
		{
			file: "pypi/pipdeptree.json",
			want: advisory.ErrNoAdvisoryForPackageType{PackageType: "pypi"},
		},
		{
			file: "yarn/yarn.lock",
			want: advisory.ErrNoPackageTypeDir{PackageType: "npm"},
		},
	}
	for _, tc := range tcs {
		t.Run(tc.file, func(t *testing.T) {
			path := filepath.Join(fixturesDir, tc.file)
			err := scanner.scanFile(path, &File{})
			require.ErrorContains(t, err, tc.want.Error())
		})
	}

	tcs = []struct {
		file string
		want error
	}{
		{
			file: "bundler/Gemfile.lock",
			want: nil,
		},
	}
	for _, tc := range tcs {
		t.Run(tc.file, func(t *testing.T) {
			path := filepath.Join(fixturesDir, tc.file)
			got := scanner.scanFile(path, &File{})
			require.Equal(t, got, tc.want)
		})
	}
}

func checkExpectedFile(t *testing.T, want, got interface{}, path string) {
	// look for file containing expected JSON document
	if _, err := os.Stat(path); err != nil {
		createExpectedFile(t, got, path)
	} else {
		compareToExpectedFile(t, want, got, path)
	}
}

func compareToExpectedFile(t *testing.T, want, got interface{}, path string) {
	f, err := os.Open(path)
	require.NoError(t, err)
	defer f.Close()

	err = json.NewDecoder(f).Decode(want)
	require.NoError(t, err)

	require.Equal(t, want, got)
}

func createExpectedFile(t *testing.T, got interface{}, path string) {
	// make test fail
	t.Errorf("creating JSON document: %s", path)

	// create directory for file
	err := os.MkdirAll(filepath.Dir(path), 0755)
	require.NoError(t, err)

	// create file
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	require.NoError(t, err)
	defer f.Close()

	// write JSON doc
	encoder := json.NewEncoder(f)
	encoder.SetIndent("", "  ")
	encoder.SetEscapeHTML(false)
	err = encoder.Encode(got)
	require.NoError(t, err)
}
