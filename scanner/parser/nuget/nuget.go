package nuget

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Document is a NuGet lock file
type Document struct {
	// Version is the format version
	Version int

	// Dependencies maps the .NET build targets to their dependencies
	Dependencies map[string]TargetDependencies
}

// TargetDependencies are all the dependencies of a .NET build target.
// This includes both direct dependencies and transitive dependencies.
type TargetDependencies map[string]DependencyInfo

// DependencyInfo describes a NuGet dependency
type DependencyInfo struct {
	// Type tells whether this is a direct project dependency.
	// It is either "direct" or "transitive".
	Type string

	// Requested is the requested version range.
	// It is set only for direct dependencies.
	Requested string

	// Resolved is the resolved version
	Resolved string

	// Dependencies is a list of transitive dependencies. It maps a package name
	// to a requested version range if the dependent package is a direct project dependency
	// (type is direct), and an exact version otherwise (type is transitive).
	Dependencies map[string]string

	// ContentHash string `json:"contentHash"`
}

const supportedFileFormatVersion = 1

// Parse scans a NuGet lock file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	// decode lock file
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}

	// check format version
	if document.Version != supportedFileFormatVersion {
		return nil, nil, parser.ErrWrongFileFormatVersion
	}

	// collect targets like ".NETCoreApp,Version=v5.0"
	targets := []string{}
	for target := range document.Dependencies {
		targets = append(targets, target)
	}

	// sort targets to get a deterministic result when resolving dependencies;
	// targets are sorted in reverse order so that for instance dependencies defined in
	// ".NETCoreApp,Version=v5.0" win over the ones defined in ".NETCoreApp,Version=v3.0"
	sort.Sort(sort.Reverse(sort.StringSlice(targets)))

	// iterate targets, collect all packages without duplicates,
	// and track packages by name to later resolve transitive dependencies
	pkgs := []parser.Package{}
	nameMap := newPkgNameMap()
	versionMap := newPkgVersionMap()
	for _, target := range targets {
		for name, info := range document.Dependencies[target] {
			// search by name and version, update map if not seen or else skip
			pkg := parser.Package{Name: name, Version: info.Resolved}
			if versionMap.seen(pkg) {
				continue
			}
			versionMap.add(&pkg)

			// add to packages
			pkgs = append(pkgs, pkg)

			// search package by name, update map if not seen or else skip;
			// the first target that has this package name wins,
			// and the package version being used is the one it defines
			if nameMap.seen(pkg) {
				continue
			}
			nameMap.add(&pkg)
		}
	}

	// iterate targets, collect top-level dependencies, and resolve and collect transitive dependencies;
	// duplicate top-level dependencies (same package name and version but different target) are skipped;
	// duplicate transitive dependencies (same dependent name and version, and same dependency name) are skipped
	deps := []parser.Dependency{}
	topLevelDepMap := newPkgVersionMap()
	transitiveDepMap := newPkgDepMap()
	for _, target := range targets {
		for name, info := range document.Dependencies[target] {
			// dependent package
			pkg := parser.Package{Name: name, Version: info.Resolved}

			// resolve dependent package
			dependent, _ := versionMap.find(pkg)

			// report as top-level dependency if Direct or Project
			switch info.Type {
			case "Direct", "Project":
				// skip if already seen
				if topLevelDepMap.seen(pkg) {
					continue
				}
				topLevelDepMap.add(&pkg)

				// append direct dependency
				deps = append(deps, parser.Dependency{
					To:           dependent,
					VersionRange: info.Requested,
				})
			default:
				// transitive dependency
			}

			// iterate transitive dependencies
			for dependencyName, requestedVersion := range info.Dependencies {
				// skip if already seen
				if transitiveDepMap.seen(pkg, dependencyName) {
					continue
				}
				transitiveDepMap.add(pkg, dependencyName)

				// resolve dependency name to package
				dependency, ok := nameMap.findByName(dependencyName)
				if !ok {
					return nil, nil, fmt.Errorf("cannot find nuget dependency: %s", dependencyName)
				}

				// append transitive dependency
				deps = append(deps, parser.Dependency{
					From:         dependent,
					To:           dependency,
					VersionRange: requestedVersion,
				})
			}
		}
	}

	return pkgs, deps, nil
}

func init() {
	parser.Register("nuget", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeNuget,
		Filenames:   []string{"packages.lock.json"},
	})
}
