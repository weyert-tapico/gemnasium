package golang

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

const (
	// FileGoSum is the defined name for file containing module to hash mappings.
	FileGoSum = "go.sum"

	// FileGoProjectModulesJSON is the defined name for the internal-only build file produced
	// by the Golang builder.
	FileGoProjectModulesJSON = "go-project-modules.json"
)

// Parse scans a go.sum or go-project-modules.json file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	file, ok := r.(*os.File)
	if !ok {
		return nil, nil, fmt.Errorf("expected io.Reader type as *os.File")
	}
	base := filepath.Base(file.Name())
	switch base {
	case FileGoProjectModulesJSON:
		log.Debugf("Selecting %q parser for %q", FileGoProjectModulesJSON, file.Name())
		return parseGoModulesJSON(r, opts)
	case FileGoSum:
		log.Warnf("Selecting %q parser for %q. False positives may occur. See https://gitlab.com/gitlab-org/gitlab/-/issues/321081.", FileGoSum, file.Name())
		return parseGoSum(r, opts)
	default:
		return nil, nil, fmt.Errorf("file %q is not supported by the golang parser", base)
	}
}

func parseGoModulesJSON(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	var modules []parser.Package
	dec := json.NewDecoder(r)
	if err := dec.Decode(&modules); err != nil {
		return nil, nil, fmt.Errorf("parsing %q: %v", err, FileGoProjectModulesJSON)
	}

	return modules, nil, nil
}

func parseGoSum(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	set := make(map[string]bool)
	result := make([]parser.Package, 0)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		parts := strings.Fields(scanner.Text())
		if len(parts) != 3 {
			log.Debugf("Skipping line in go.sum: %q", parts)
			continue
		}
		name, version := parts[0], parts[1]
		version = strings.TrimSuffix(version, "/go.mod")
		version = strings.TrimSuffix(version, "+incompatible")
		id := fmt.Sprintf("%s:%s", name, version)
		if set[id] {
			continue
		}
		result = append(result, parser.Package{Name: name, Version: version})
		set[id] = true
	}
	return result, nil, nil
}

func init() {
	parser.Register("go", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeGo,
		Filenames:   []string{FileGoProjectModulesJSON, FileGoSum},
	})
}
