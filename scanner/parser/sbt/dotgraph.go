package sbt

import (
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/encoding/dot"
	"gonum.org/v1/gonum/graph/simple"
)

// dotGraph is the simple.DirectedGraph representation of a dot file
type dotGraph struct {
	*simple.DirectedGraph
}

// newDotGraph returns a new directed graph representing a project's
// dependencies and their relationships
func newDotGraph(bs []byte) (*dotGraph, error) {
	dg := dotGraph{DirectedGraph: simple.NewDirectedGraph()}
	err := dot.Unmarshal(bs, &dg)
	return &dg, err
}

// NewNode returns a new dotNode wrapping a graph.Node
func (g *dotGraph) NewNode() graph.Node {
	return &dotNode{Node: g.DirectedGraph.NewNode()}
}

// NewEdge returns a new dotEdge between the from and to nodes
func (g *dotGraph) NewEdge(from, to graph.Node) graph.Edge {
	return &dotEdge{Edge: g.DirectedGraph.NewEdge(from, to)}
}

// SetEdge sets the edge only if from and to nodes are different,
// preventing errors when attempting to set self edges.
func (g *dotGraph) SetEdge(e graph.Edge) {
	if e.From() == e.To() {
		return
	}
	g.DirectedGraph.SetEdge(e)
}

// dotNode extends simple.Node and stores an id representing the
// packageName and version of the package
type dotNode struct {
	graph.Node
	id        string
	evictedBy *dotNode
}

// SetDOTID sets the id for the node as represented by the edge label
// dependency dot ids come in the form of <group>:<package>:<version>
func (n *dotNode) SetDOTID(id string) {
	n.id = id
}

// dotEdge extends simple.Edge and stores the directed dependency
// relationship between packages
type dotEdge struct {
	graph.Edge
	evictionEdge bool
}

// SetAttribute is an interface func that is used by the dot decoder
// to set attributes on encountered edges
func (e *dotEdge) SetAttribute(a encoding.Attribute) error {
	if a.Key == "label" && a.Value == "Evicted By" {
		from := e.From().(*dotNode)
		to := e.To().(*dotNode)
		from.evictedBy = to
		e.evictionEdge = true
	}
	return nil
}
