#!/usr/bin/env sh

SPEC_FILE=${SPEC_FILE:-"spec/image_spec.rb"}

RSPEC_FORMATTER="./lib/rspec/core/formatters/parallel_group_extractor.rb"
RSPEC_FORMAT="ParallelGroupExtractor"

set -e -x

# Run rspec to get the locations of rspec contexts where "run_parallel" is true.
#
# If the environment variables CI_NODE_INDEX and CI_NODE_TOTAL are set,
# it selects a bucket of locations for the requested CI node.
#
# The output is formatted so that it can be directly passed to the rspec CLI.
# This is a space-separated list of locations, and each line is a file path
# and line number separated by a semi-colon.
# Example: spec/image_spec.rb:100 spec/image_spec.rb:200
#
# rspec is executed in dry-run mode, and the examples aren't executed.
#
# The line where rspec shows the "Run options" is removed to only keep the locations.
#
locations=$(rspec --require ${RSPEC_FORMATTER} --format ${RSPEC_FORMAT} --dry-run ${SPEC_FILE}|grep -v "Run options")

# Run rspec with the selected locations
rspec --format documentation $locations
