package flags_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cli/flags"
)

func TestFlags(t *testing.T) {
	t.Run("New", func(t *testing.T) {
		t.Run("When no arguments are passed it returns the default set of flags", func(t *testing.T) {
			actual := flags.New()
			require.Equal(t, flags.DefaultFlags, actual)
		})

		t.Run("When flags with different names are passed it combines all the flags", func(t *testing.T) {
			flags1 := []cli.Flag{
				&cli.StringFlag{
					Name:    "flag-1",
					Usage:   "Flag One",
					EnvVars: []string{"FLAG_ONE"},
				},
			}

			flags2 := []cli.Flag{
				&cli.StringFlag{
					Name:    "flag-2",
					Usage:   "Flag Two",
					EnvVars: []string{"FLAG_TWO"},
				},
			}

			expected := []cli.Flag{
				flags.DefaultFlags[0],
				flags.DefaultFlags[1],
				flags1[0],
				flags2[0],
			}

			actual := flags.New(flags1, flags2)
			require.Equal(t, expected, actual)
		})
	})
}
