package sbom

import (
	"errors"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cli/flags"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/keystore"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

type buildProjectsFn func(isExcludedPathFn finder.IsExcludedPathFunc, projects *[]finder.Project, targetDir string) error

// Command defines the sbom command which is used to generate an SBOM file
func Command(cliflags []cli.Flag, finderPreset finder.Preset, buildProjects buildProjectsFn) *cli.Command {
	return &cli.Command{
		Name:    "sbom",
		Aliases: []string{"s"},
		Usage:   "Run the analyzer on detected project and generate a compatible SBOM",
		Flags:   cliflags,
		Action: func(c *cli.Context) error {
			startTime := time.Now()

			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// configure version range resolvers
			if err := vrange.Configure(c); err != nil {
				return err
			}

			// the binary executed by `keystore.Update()` doesn't exist in the gemnasium and
			// gemnasium-python images, so we only want to execute this for gemnasium-maven
			if finderPreset == finder.PresetGemnasiumMaven {
				// configure CA certificates
				if err := keystore.Update(c); err != nil {
					return err
				}
			}

			// configure builders
			if err := builder.Configure(c); err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flags.TargetDir))
			if err != nil {
				return err
			}

			// find supported projects
			find, err := finder.NewFinder(c, finderPreset)
			if err != nil {
				return err
			}
			projects, err := find.FindProjects(targetDir)
			if err != nil {
				return err
			}

			// raise warning when there is nothing to scan
			if len(projects) == 0 {
				// mimic search.ErrNotFound error of common/search
				log.Warnf("No match in %s", targetDir)
				return nil
			}

			if buildProjects != nil {
				if err := buildProjects(find.IsExcludedPath, &projects, targetDir); err != nil {
					return err
				}
			}

			// scan target directory
			scanner, err := scanner.NewScanner(c)
			if err != nil {
				return err
			}
			result, err := scanner.ScanProjects(targetDir, projects)
			if err != nil {
				return err
			}

			toolInfo := cyclonedx.ToolInfo{Name: metadata.AnalyzerName, Vendor: metadata.AnalyzerVendor, Version: metadata.AnalyzerVersion}

			// generate SBOMs and manifest
			cycloneDXSBOMs := cyclonedx.ToSBOMs(result, &startTime, toolInfo)
			return cyclonedx.OutputSBOMs(targetDir, cycloneDXSBOMs)
		},
	}
}
