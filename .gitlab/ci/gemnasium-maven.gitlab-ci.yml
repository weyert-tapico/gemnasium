variables:
  # Temporary image: registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium/tmp/maven
  # Local image:     registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium/maven
  # Official image:  registry.gitlab.com/security-products/gemnasium-maven
  IMAGE_ALIAS: "maven"
  ANALYZER: "gemnasium-maven"

  SEC_REGISTRY_IMAGE:  "$GEMNASIUM_MAVEN_SEC_REGISTRY_IMAGE"
  SEC_REGISTRY_USER: "$GEMNASIUM_MAVEN_SEC_REGISTRY_USER"
  SEC_REGISTRY_PASSWORD: "$GEMNASIUM_MAVEN_SEC_REGISTRY_PASSWORD"

  MAX_IMAGE_SIZE_MB: 1750
  MAX_IMAGE_SIZE_MB_FIPS: 780
  SCAN_DURATION_MARGIN_PERCENT: 25

include:
  local: "/.gitlab/ci/image.gitlab-ci.yml"

cache:
  paths:
    - .gradle/wrapper

build tmp image:
  variables:
    DOCKERFILE: "build/$ANALYZER/debian/Dockerfile"

build tmp image fips:
  variables:
    DOCKERFILE: "build/$ANALYZER/redhat/Dockerfile"

image test:
  timeout: "20 minutes"
  script:
    # skip rspec examples for RedHat build (FIPS)
    - rspec -f d --tag ~run_parallel --tag ~build:redhat spec/gemnasium-maven_image_spec.rb

image test fips:
  script:
    # skip rspec examples for Debian build (not FIPS)
    - rspec -f d --tag ~run_parallel --tag ~build:debian spec/gemnasium-maven_image_spec.rb

image test only slow scans:
  extends: image test
  variables:
    SPEC_FILE: "spec/gemnasium-maven_image_spec.rb"
  before_script:
    # skip rspec examples for RedHat build (FIPS)
    - echo "overriding rspec configuration file to filter out contexts specific to RedHat (FIPS)"
    - echo "--tag ~build:redhat" > ./.rspec
  script:
    - ./test_runner.sh
  parallel: 20

image test only slow scans fips:
  extends: image test only slow scans
  variables:
    IMAGE_TAG_SUFFIX: "-fips"
  before_script:
    # skip rspec examples for Debian build (not FIPS)
    - echo "overriding rspec configuration file to filter out contexts specific to Debian (not FIPS)"
    - echo "--tag ~build:debian" > ./.rspec
  parallel: 16

.functional:
  extends: .qa-downstream-ds
  variables:
    DS_EXCLUDED_ANALYZERS: "gemnasium,gemnasium-python,bundler-audit,retire.js"

java-maven-offline-qa:
  extends: .functional
  variables:
    EXPECTATION: "java-maven/default"
    EXPECTED_CYCLONEDX_ARTIFACTS: "gl-sbom-maven-maven.cdx.json"
  trigger:
    project: gitlab-org/security-products/tests/java-maven
    branch: offline-FREEZE

# TODO: re-enable this test. Disabling this test temporarily while we investigate timeout issues
# java-maven-offline-qa fips:
#   extends:
#     - .qa-fips
#     - java-maven-offline-qa

# check to ensure that the Dependency-Scanning.gitlab-ci.yml template correctly saves cyclonedx-*.json files
java-gradle-qa:
  extends: .functional
  variables:
    # our java-gradle test project uses a version of gradle which is incompatible with Java 17, so we force Java 11 here
    DS_JAVA_VERSION: "11"
    EXPECTATION: "java-gradle/default"
    EXPECTED_CYCLONEDX_ARTIFACTS: "gl-sbom-maven-gradle.cdx.json"
  trigger:
    project: gitlab-org/security-products/tests/java-gradle

# TODO: re-enable this test. Disabling this test temporarily while we investigate timeout issues
# scala-sbt-qa fips:
#   extends:
#     - .functional
#     - .qa-fips
#   variables:
#     EXPECTATION: "scala-sbt/default"
#   trigger:
#     project: gitlab-org/security-products/tests/scala-sbt
