workflow:
 rules:
   # When a new tag is created, run a pipeline.
   - if: $CI_COMMIT_TAG
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, manually triggering a master pipeline, etc.).
   - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # For merge requests, create a pipeline.
   - if: $CI_MERGE_REQUEST_ID

variables:
  GENERATE_IMAGE_SBOM: "true"

  # SETTINGS
  #
  # Alias for the image,  like "main" or "python".
  # It's used in the temporary image name,
  # like "gemnasium/tmp/python",
  # and in the local image name, like "gemnasium/python".
  IMAGE_ALIAS: ""

  # Name of the analyer, like "gemnasium-python".
  # This is the name of the image published
  # to registry.gitlab.com/security-products,
  # and the name of the directory that contains the Dockerfile.
  ANALYZER: ""

  # SHARED VARIABLES
  TMP_IMAGE_PATH: "/tmp/$IMAGE_ALIAS"
  IMAGE_PATH: "/$IMAGE_ALIAS"
  SEC_REGISTRY_IMAGE: "registry.gitlab.com/security-products/$ANALYZER"

stages:
  - pre-build
  - build-image
  - test
  - release-version
  - release-major

include:
  - project: 'gitlab-org/security-products/ci-templates'
    ref: 'master'
    file: '/includes-dev/docker.yml'
  - project: 'gitlab-org/security-products/ci-templates'
    ref: 'master'
    file: '/includes-dev/docker-test.yml'

build tmp image:
  variables:
    DOCKERFILE: "build/$ANALYZER/Dockerfile"

build tmp image fips:
  variables:
    DOCKERFILE: "build/$ANALYZER/Dockerfile.fips"

# run container scanning in parent pipeline to collect security reports
# TODO: remove this job once reports can be read from child pipelines;
# see https://gitlab.com/gitlab-org/gitlab/-/issues/215725
container_scanning:
  rules:
    - when: never

container_scanning-fips:
  rules:
    - when: never

image test:
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/integration-test:stable
  stage: test
  timeout: 10m
  services:
    - docker:20.10-dind
  variables:
    IMAGE_TAG_SUFFIX: ""
    TMP_IMAGE: "$CI_REGISTRY_IMAGE$TMP_IMAGE_PATH:$CI_COMMIT_SHA$IMAGE_TAG_SUFFIX"
  script:
    - rspec -f d spec/${ANALYZER}_image_spec.rb
  artifacts:
    when: always
    paths:
      - tmp/test-*/**/gl-dependency-scanning-report.json
      - tmp/test-*/**/gl-sbom-*.cdx.json
      - tmp/test-*/**/sbom-manifest.json

image test fips:
  extends: image test
  variables:
    IMAGE_TAG_SUFFIX: "-fips"

test-custom-ca-bundle:
  variables:
    RUN_CMD: 1
    SEARCH_CMD: 0
    ANALYZE_CMD: 0
    CONVERT_CMD: 0

.functional:
  extends: .qa-downstream-ds
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/$EXPECTATION/gl-dependency-scanning-report.json"

.qa-fips:
  variables:
    CI_GITLAB_FIPS_MODE: "true"
    DS_JOB_TAG: "fips"
